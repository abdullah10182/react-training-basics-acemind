import React from 'react';
import { render } from 'react-dom';

import { Header } from "./components/Header";
import { Home } from "./components/Home";

import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class App extends React.Component {
  constructor() {
    super();
    this.state ={
      homeLink: 'home',
      homeMounted: true
    };
  }

  onGreet(){
    alert('hello');
  }

  onChangeLinkName(newName) {
    this.setState({
      homeLink: newName
    });
  }

  onChangeHomeMounted(){
    this.setState({
      homeMounted: !this.state.homeMounted
    })
  }

  render(){
    let homeCmp = "";
    if(this.state.homeMounted){
      homeCmp = (
      <Home 
        name={"abdullah"} 
        initialAge={36} 
        greet={this.onGreet} 
        changeLink={this.onChangeLinkName.bind(this)}
        initialLinkName={this.state.homeLink}
      />)
    }
    return (
      <div className="container">
        <div className="row card card-block bg-faded">
          <Header homeLink={this.state.homeLink}/>
        </div>
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}>

            {homeCmp}
    
        </ReactCSSTransitionGroup>
        <hr/>
        <button className="btn btn-primary" onClick={this.onChangeHomeMounted.bind(this)}>Unmount home comp</button>
      </div>
    );
  }
}

render(<App/>, window.document.getElementById('app'));


module.hot.accept();