import React from 'react';
import PropTypes from 'prop-types';

export class Home extends React.Component {
    constructor(props){
        super();
        this.state = {
            age: props.initialAge,
            status: 0,
            homeLink: props.initialLinkName
        }
        setTimeout(() => {
            this.setState({
                status: 1
            });
        }, 1000);
    }

    onMakeOlder(){
        this.setState({
            age: this.state.age + 3
        });
    }

    onChangeLink() {
       
        this.props.changeLink(this.state.homeLink);
    }

    onHandleChange(event) {
        this.setState({ homeLink: event.target.value }, () => {
            this.onChangeLink();
        }); 

    }

    componentWillMount() {
      console.log('Component will mount');
    }

    componentDidMount () {
      console.log('component did mount');
    }

    componentWillReceiveProps(nextProps){
        console.log('component will recieve props', nextProps);
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        console.log("should compo update", nextProps, nextState);
        return true;
    }

    componentWillUpdate(nextProps, nextState){
        console.log('component will update', nextProps, nextState);
    }
    
    componentDidUpdate(prevProps, prevState){
        console.log('componentDidUpdate', prevProps, prevState);
    }

    componentWillUnmount(){
        console.log('compent wil umount');
    }
    
    render() {
        return(
            <div>
                <p>in new component</p>
                <div>
                    {this.props.name}
                    {this.state.age}
                </div>
                <div>
                    {this.props.name}
                    {this.state.age}
                </div>
                <hr/>
                <button className="btn btn-primary" onClick={this.onMakeOlder.bind(this)}>Make me older</button>
                <hr/>
                <button className="btn btn-primary" onClick={this.props.greet}>greet</button>
                <div>{this.state.homeLink}</div>
                
                <hr/>
                <input 
                    type="text" 
                    value={this.state.homeLink} 
                    onChange={(event)=> this.onHandleChange(event)} 
                />
                <button className="btn btn-primary" onClick={this.onChangeLink.bind(this)}>change header link</button>


            </div>
        );
    }
}

Home.propTypes = {
    name: PropTypes.string.isRequired,
    age: PropTypes.number,
    greet: PropTypes.func,
    initialLinkName: PropTypes.string
};